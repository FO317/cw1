## PDE4420 Course work 1

This project is developed for my CW1.

---
AIM
To demostrate the ability to pass sensors values from arduino board to ROS which will show progress in understanding the course.

MATERIALS

HARDWARE

Arduino board
Jumper cables
Bread board
Ultrasonic sensor HC-SR04

SOFTWARE:

ROS
ARDUINO IDE

SCOPE: 

Connect the arduino as depleted  in the location below:

https://i2.wp.com/randomnerdtutorials.com/wp-content/uploads/2013/11/ultrasonic-sensor-with-arduino-hc-sr04.jpg?ssl=1 (Copied From Arduino Tutorial).

HOW IT WORKS:

ROSSERIAL ensured installed in the ROS that serves as publisher node for arduino. 
By running roscore and  ROSSERIAL node in the ROS, the sensor values will be obtained in ROS.
